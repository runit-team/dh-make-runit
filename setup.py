#!/usr/bin/env python3
from setuptools import setup, find_packages

setup(
    name     = 'dh-make-runit',
    version  = '0.1',
    url      = 'https://salsa.debian.org/runit-team/dh-make-runit',
    maintainer = 'Dmitry Bogatov',
    maintainer_email = 'KAction@gnu.org',
    packages = find_packages(),
    entry_points = {
        'console_scripts' : [
            'dh-make-runit = runit.main.runpackage:main'
            ]
        },
    test_suite = "runit.tests"
    )
