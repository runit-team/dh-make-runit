# Copyright (C) 2017 Dmitry Bogatov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""Create Debian source package for runit service script.

Source directory name has form <foo>-run, if server is provided by
package bin:<foo>.  Naming convention started by Gerrit Pape and
codified, more or less, by 'bin:dh-runit' and 'bin:getty-run' (from
'src:runit').

Maintainer of server <foo> is backed by Debian Policy to be fully
content with only SysV initscript, and just 'tag:wontfix' requests for
support of Runit initialization system. Hence the idea of providing
runscripts in separate source package, and hence this program.

As much as possible information is prepared automatically, including
deriving description from description of <foo> package; generated
package should be lintian-clean, but minimal human checks are
recommended.

"""

import argparse
import sys
import os
import os.path as path
from collections import namedtuple
import types
from runit.extra import argument
import apt_pkg as apt
from datetime import datetime, timezone

description = namedtuple('description', 'short,long')

def write_txt(filename, text):
    """Overwrite filename with given text.

    Intermediate directories are created as needed, trailing newline
    is added.

    """
    os.makedirs(path.dirname(filename), exist_ok=True)
    if text[-1] != '\n':
        text += '\n'
    with open(filename, 'w') as f:
        f.write(text)

class debj2:
    """Wrapper around jinja2, tailored for needs of this script."""

    import jinja2
    _env = jinja2.Environment(loader=jinja2.PackageLoader('runit', 'templates'))

    def write(debian, mapping):
        """Render templates into files under given debian/ directory.

        Specified debian directory must already exist and its content
        will be overwritten without prompt. Templates are instantiated
        with provided mapping."""

        for fname in ['control', 'rules', 'changelog', 'copyright']:
            templatename = 'runpackage.%s.j2' % fname
            template = debj2._env.get_template(templatename)
            write_txt(path.join(debian, fname), template.render(**mapping))
        os.chmod(path.join(debian, 'rules'), 0o755)

        write_txt(path.join(debian, 'source/format'), '3.0 (native)')


def validate_sourcedir(string):
    """Validate source directory argument.

    If must be either non-existent, or do not have debian/
    subdirectory.  In either case its full name must end with '-run'.

    Return absolute path to directory.

    >>> validate_sourcedir('/tmp/foo-run')
    '/tmp/foo-run'

    >>> validate_sourcedir('/tmp/foo-withoutrun')
    Traceback (most recent call last):
        ...
    argparse.ArgumentTypeError: '/tmp/foo-withoutrun' full path does not ends with '-run'
    """

    directory = path.abspath(string)
    if not directory.endswith("-run"):
        msg = "%r full path does not ends with '-run'" % string
        raise argparse.ArgumentTypeError(msg)

    debian = path.join(string, 'debian')
    if path.exists(debian):
        msg = "%r already exists" % debian
        raise argparse.ArgumentTypeError(msg)

    return directory

def commandline():
    'Parse command line options and return corresponding namespace object.'
    fmt = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=fmt)
    parser.add_argument('sourcedir', nargs='?', default='.',
                            type=validate_sourcedir,
                            help='where create source package'
                                 ' (default: current directory)')
    argument.maintainer(parser)

    defuri = 'https://salsa.debian.org/runit-team/{name}-run'
    urihelp = """URI of Git repository for source package.

    It can contain {name} substring, that will be substituted with
    name of server package. Default value referes to runit-team
    project on Salsa.

    """

    parser.add_argument('-u', '--uri', default=defuri, help=urihelp)
    return parser.parse_args()

def server_name(sourcedir):
    """Return the corresponding server binary package name.

    >>> server_name('/tmp/foo-run')
    'foo'
    >>> server_name('bar-run')
    'bar'
    >>> server_name('qq')
    Traceback (most recent call last):
        ...
    AssertionError

    """

    assert(sourcedir.endswith('-run'))
    return path.basename(sourcedir)[0:-4]

def apt_extract_description(name):
    """Return named tuple (short, long) with descriptions of given binary package."""
    apt.init()
    cache     = apt.Cache(progress=None)
    candidate = apt.DepCache(cache).get_candidate_ver(cache[name])
    records   = apt.PackageRecords(cache)
    records.lookup(candidate.translated_description.file_list[0])

    short = records.short_desc
    # Somewhy first line is part of long description. Remove it.
    long = '\n'.join(records.long_desc.split('\n')[1:])

    return description(short, long)

def main():
    info = commandline()
    info.name = server_name(info.sourcedir)
    info.uri = info.uri.format(name=info.name)
    info.date = types.SimpleNamespace()
    today = datetime.now(timezone.utc)
    info.date.year = today.strftime('%Y')
    info.date.rfc2822 = today.strftime('%a, %d %b %Y %H:%M:%S %z')
    try:
        info.description = apt_extract_description(info.name)
    except KeyError:
        sys.stderr.write('Error: Debian package %r does not exist\n' % info.name)
        sys.exit(1)

    debian = path.join(info.sourcedir, 'debian')
    os.makedirs(debian, exist_ok=True)
    debj2.write(debian, vars(info))

    dh_runit = types.SimpleNamespace()
    dh_runit.path = path.join(debian, '{name}-run.runit'.format(name=info.name))
    dh_runit.content = '{name} logscript'.format(name=info.name)
    write_txt(dh_runit.path, dh_runit.content)

    runscript = types.SimpleNamespace()
    runscript.path = path.join(info.sourcedir, info.name)
    runscript.content = '#!/bin/sh\necho Implement me!\nexit 1'
    write_txt(runscript.path, runscript.content)
    os.chmod(runscript.path, 0o755)

    print("""Seems fine. At least following should be done manually:

  1. Edit {sourcedir}/debian/changelog and write ITP number.
  2. Edit {sourcedir}/{name}.runscript and implement runscript.
     Several lines should be enough.

Good luck!""".format(sourcedir=info.sourcedir, name=info.name))

if __name__ == '__main__':
    import doctest
    doctest.testmod()
