# Copyright (C) 2017 Dmitry Bogatov
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from collections import namedtuple
import os
import argparse
import re

# Just save typing. 80 columns are sacred.
def _errtype(msg):
    """Raise argparse.ArgumentTypeError with given message."""
    raise argparse.ArgumentTypeError(msg)

def _maintainer_validate(string):
    """Return named tuple, containing name and email of maintainer.

    If argument is None, extract information from environment variables.
    Raise ArgumentTypeError if either argument is malformed or required
    environment variables are not set.

    >>> _maintainer_validate('J. Random Hacker <Hacker@example.com>')
    maintainer(name='J. Random Hacker', email='Hacker@example.com')
    >>> _maintainer_validate('noname@example.com')
    Traceback (most recent call last):
        ...
    argparse.ArgumentTypeError: 'noname@example.com': malformed maintainer info

    >>> _maintainer_validate('<noname@example.com>')
    Traceback (most recent call last):
        ...
    argparse.ArgumentTypeError: '<noname@example.com>': empty name

    >>> import os
    >>> _maintainer_validate('\a')
    Traceback (most recent call last):
        ...
    argparse.ArgumentTypeError: DEBFULLNAME is not set

    >>> os.environ['DEBFULLNAME'] = 'J.Hacker'
    >>> _maintainer_validate('\a')
    Traceback (most recent call last):
        ...
    argparse.ArgumentTypeError: DEBEMAIL is not set

    >>> os.environ['DEBEMAIL'] = 'hacker@example.com'
    >>> _maintainer_validate('\a')
    maintainer(name='J.Hacker', email='hacker@example.com')

    """
    if string == '\a':
        name = os.getenv('DEBFULLNAME')
        if name is None:
            _errtype('DEBFULLNAME is not set')
        email = os.getenv('DEBEMAIL')
        if email is None:
            _errtype('DEBEMAIL is not set')
        return _maintainer_cls(name, email)
    else:
        try:
            (name, email, _) = re.split(r'[ ]?[<>]', string)
        except ValueError:
            _errtype('%r: malformed maintainer info' % string)

        if len(name) == 0:
            _errtype('%r: empty name' % string)

        if email.count('@') != 1:
            _errtype('%r: mailformed email' % string)
        return _maintainer_cls(name, email)

_maintainer_cls = namedtuple('maintainer', 'name,email')
_maintainer_keys = ('--maintainer', '-m')
_maintainer_help = """package maintainer in form `Name <email@example.com>'.

By default, DEBFULLNAME and DEBEMAIL environment variables are
used. They must be set in such case."""

def maintainer(parser, keys=_maintainer_keys, help=_maintainer_help):
    """Add Debian maintainer argument to given parser."""
    parser.add_argument(*keys, help=help, type=_maintainer_validate, default='\a')

if __name__ == '__main__':
    import doctest
    import os
    del os.environ['DEBFULLNAME']
    del os.environ['DEBEMAIL']
    doctest.testmod()
